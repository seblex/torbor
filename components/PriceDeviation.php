<?php

namespace app\components;

use yii\db\Exception;

/**
 *
 */
class PriceDeviation
{
    /**
     * Разрешённое отклонение
     */
    protected $permissionVariable;

    /**
     * Текущая цена
     */
    protected $currentPrice;

    /**
     * Предыдущая цена
     */
    protected $previousPrice = 0;

    /**
     * Результат вычислений - отклонение
     */
    protected $result;

    /**
     * Конструктор
     *
     * @param $variable - отклонение
     * @param $currentPrice - текущая цена
     * @throws
     */
    public function __construct($variable, $currentPrice)
    {
        try {
            $this->permissionVariable = $variable;
            $this->currentPrice = $currentPrice;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Сеттер для предыдущей цены
     *
     * @param $price - предыдущая цена
     */
    public function setPreviousPrice($price)
    {
        $this->previousPrice = $price;
    }

    /**
     * Проверка соответствия отклонения
     *
     * @return bool
     */
    public function diff()
    {
        $diff = abs($this->currentPrice - $this->previousPrice);
        $result = ($diff * 100)/$this->previousPrice;
        $this->result = $result;
        if ($result > $this->permissionVariable) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Получение результата отклонения
     */
    public function getResult()
    {
        return $this->result;
    }
}